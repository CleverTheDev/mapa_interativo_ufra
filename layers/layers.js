var wms_layers = [];
var baseLayer = new ol.layer.Group({
    'title': '',
    layers: [
new ol.layer.Tile({
    'title': 'OSM DE',
    'type': 'base',
    source: new ol.source.XYZ({
        url: 'https://a.tile.openstreetmap.org/{z}/{x}/{y}.png',
        attributions: [new ol.Attribution({html: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors,<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'})]
    })
})
]
});
var format_Calada_0 = new ol.format.GeoJSON();
var features_Calada_0 = format_Calada_0.readFeatures(json_Calada_0,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Calada_0 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_Calada_0.addFeatures(features_Calada_0);var lyr_Calada_0 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_Calada_0,
                style: style_Calada_0,
                title: '<img src="styles/legend/Calada_0.png" /> Calçada'
            });var format_Vias_1 = new ol.format.GeoJSON();
var features_Vias_1 = format_Vias_1.readFeatures(json_Vias_1,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Vias_1 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_Vias_1.addFeatures(features_Vias_1);var lyr_Vias_1 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_Vias_1,
                style: style_Vias_1,
                title: '<img src="styles/legend/Vias_1.png" /> Vias'
            });var format_DrenagemComposta_2 = new ol.format.GeoJSON();
var features_DrenagemComposta_2 = format_DrenagemComposta_2.readFeatures(json_DrenagemComposta_2,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_DrenagemComposta_2 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_DrenagemComposta_2.addFeatures(features_DrenagemComposta_2);var lyr_DrenagemComposta_2 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_DrenagemComposta_2,
                style: style_DrenagemComposta_2,
                title: '<img src="styles/legend/DrenagemComposta_2.png" /> Drenagem Composta'
            });var format_CampodeFutebol_3 = new ol.format.GeoJSON();
var features_CampodeFutebol_3 = format_CampodeFutebol_3.readFeatures(json_CampodeFutebol_3,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_CampodeFutebol_3 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_CampodeFutebol_3.addFeatures(features_CampodeFutebol_3);var lyr_CampodeFutebol_3 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_CampodeFutebol_3,
                style: style_CampodeFutebol_3,
                title: '<img src="styles/legend/CampodeFutebol_3.png" /> Campo de Futebol'
            });var format_Edificaes_4 = new ol.format.GeoJSON();
var features_Edificaes_4 = format_Edificaes_4.readFeatures(json_Edificaes_4,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Edificaes_4 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_Edificaes_4.addFeatures(features_Edificaes_4);var lyr_Edificaes_4 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_Edificaes_4,
                style: style_Edificaes_4,
                title: '<img src="styles/legend/Edificaes_4.png" /> Edificações'
            });var format_DrenagemSimples_5 = new ol.format.GeoJSON();
var features_DrenagemSimples_5 = format_DrenagemSimples_5.readFeatures(json_DrenagemSimples_5,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_DrenagemSimples_5 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_DrenagemSimples_5.addFeatures(features_DrenagemSimples_5);var lyr_DrenagemSimples_5 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_DrenagemSimples_5,
                style: style_DrenagemSimples_5,
                title: '<img src="styles/legend/DrenagemSimples_5.png" /> Drenagem Simples'
            });var format_CercaseMuros_6 = new ol.format.GeoJSON();
var features_CercaseMuros_6 = format_CercaseMuros_6.readFeatures(json_CercaseMuros_6,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_CercaseMuros_6 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_CercaseMuros_6.addFeatures(features_CercaseMuros_6);var lyr_CercaseMuros_6 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_CercaseMuros_6,
                style: style_CercaseMuros_6,
                title: '<img src="styles/legend/CercaseMuros_6.png" /> Cercas e Muros'
            });var format_LimiteUfra_7 = new ol.format.GeoJSON();
var features_LimiteUfra_7 = format_LimiteUfra_7.readFeatures(json_LimiteUfra_7,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_LimiteUfra_7 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_LimiteUfra_7.addFeatures(features_LimiteUfra_7);var lyr_LimiteUfra_7 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_LimiteUfra_7,
                style: style_LimiteUfra_7,
                title: '<img src="styles/legend/LimiteUfra_7.png" /> Limite Ufra'
            });

lyr_Calada_0.setVisible(true);lyr_Vias_1.setVisible(true);lyr_DrenagemComposta_2.setVisible(true);lyr_CampodeFutebol_3.setVisible(true);lyr_Edificaes_4.setVisible(true);lyr_DrenagemSimples_5.setVisible(true);lyr_CercaseMuros_6.setVisible(true);lyr_LimiteUfra_7.setVisible(true);
var layersList = [baseLayer,lyr_Calada_0,lyr_Vias_1,lyr_DrenagemComposta_2,lyr_CampodeFutebol_3,lyr_Edificaes_4,lyr_DrenagemSimples_5,lyr_CercaseMuros_6,lyr_LimiteUfra_7];
lyr_Calada_0.set('fieldAliases', {'Id': 'Id', });
lyr_Vias_1.set('fieldAliases', {'Nome': 'Nome', });
lyr_DrenagemComposta_2.set('fieldAliases', {'Id': 'Id', 'Nome': 'Nome', });
lyr_CampodeFutebol_3.set('fieldAliases', {'Nome': 'Nome', 'Link': 'Link', });
lyr_Edificaes_4.set('fieldAliases', {'Nome': 'Nome', 'link': 'link', });
lyr_DrenagemSimples_5.set('fieldAliases', {'Igarapé': 'Igarapé', });
lyr_CercaseMuros_6.set('fieldAliases', {'Id': 'Id', 'Tipo': 'Tipo', });
lyr_LimiteUfra_7.set('fieldAliases', {'Área': 'Área', 'Perímetro': 'Perímetro', });
lyr_Calada_0.set('fieldImages', {'Id': 'Hidden', });
lyr_Vias_1.set('fieldImages', {'Nome': 'TextEdit', });
lyr_DrenagemComposta_2.set('fieldImages', {'Id': 'Hidden', 'Nome': 'TextEdit', });
lyr_CampodeFutebol_3.set('fieldImages', {'Nome': 'TextEdit', 'Link': 'Photo', });
lyr_Edificaes_4.set('fieldImages', {'Nome': 'TextEdit', 'link': 'Photo', });
lyr_DrenagemSimples_5.set('fieldImages', {'Igarapé': 'TextEdit', });
lyr_CercaseMuros_6.set('fieldImages', {'Id': 'Hidden', 'Tipo': 'TextEdit', });
lyr_LimiteUfra_7.set('fieldImages', {'Área': 'TextEdit', 'Perímetro': 'TextEdit', });
lyr_Calada_0.set('fieldLabels', {});
lyr_Vias_1.set('fieldLabels', {'Nome': 'header label', });
lyr_DrenagemComposta_2.set('fieldLabels', {'Nome': 'header label', });
lyr_CampodeFutebol_3.set('fieldLabels', {'Nome': 'header label', 'Link': 'no label', });
lyr_Edificaes_4.set('fieldLabels', {'Nome': 'header label', 'link': 'no label', });
lyr_DrenagemSimples_5.set('fieldLabels', {'Igarapé': 'header label', });
lyr_CercaseMuros_6.set('fieldLabels', {'Tipo': 'header label', });
lyr_LimiteUfra_7.set('fieldLabels', {'Área': 'header label', 'Perímetro': 'header label', });
lyr_LimiteUfra_7.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});
